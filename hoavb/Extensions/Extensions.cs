﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace hoavb
{
    public static class Extensions
    {
        public static string AddClass(this string htmlContent, string selector, string classValue)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            if (doc.DocumentNode.SelectNodes(selector) != null)
            {
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes(selector))
                {
                    var attr = node.Attributes["class"];
                    if (attr == null)
                    {
                        node.Attributes.Add("class", classValue);
                    }
                    else
                    {
                        if (!attr.Value.Contains(classValue))
                        {
                            attr.Value = (attr.Value + " " + classValue).Trim();
                        }
                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }

        public static string ReplaceInnerHtml(this string htmlContent, string selector, string find, string replace)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            if (doc.DocumentNode.SelectNodes(selector) != null)
            {
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes(selector))
                {
                    node.InnerHtml = node.InnerHtml.Replace(find, replace);
                }
            }

            return doc.DocumentNode.OuterHtml;
        }


        public static string MoveFirstRowToHead(this string htmlContent, string tableSelector)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            if (doc.DocumentNode.SelectNodes(tableSelector) != null)
            {

                foreach (HtmlNode table in doc.DocumentNode.SelectNodes(tableSelector))
                {
                    HtmlNode tHead = table.SelectSingleNode("//thead");
                    if(tHead == null) {
                        table.PrependChild(doc.CreateElement("thead"));
                        tHead = table.SelectSingleNode("//thead");
                    }

                    HtmlNode firstRow = table.SelectSingleNode("//tr");
                    if (firstRow != null)
                    {
                        string rowHtml = firstRow.InnerHtml;
                        table.RemoveChild(firstRow, false);
                        firstRow = doc.CreateElement("tr");
                        firstRow.InnerHtml = rowHtml;
                        tHead.AppendChild(firstRow);

                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }


        public static string RemoveClass(this string htmlContent, string selector, string classValue)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            if (doc.DocumentNode.SelectNodes(selector) != null)
            {
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes(selector))
                {
                    var attr = node.Attributes["class"];
                    if (attr == null)
                    {
                        return htmlContent;
                    }
                    else
                    {
                        if (attr.Value.Contains(classValue))
                        {
                            attr.Value = attr.Value.Replace(classValue, "").Trim();
                        }
                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }

        public static string RemoveNodes(this string htmlContent, string selector)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes(selector).ToList())
            {
                node.Remove();
            }

            return doc.DocumentNode.OuterHtml;
        }


        public static string SetAttribute(this string htmlContent, string selector, string attributeName, string attributeValue)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            if (doc.DocumentNode.SelectNodes(selector) != null)
            {
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes(selector).ToList())
                {
                    if (node.HasAttributes)
                    {
                        var attr = node.Attributes[attributeName];
                        if (attr == null)
                        {
                            node.Attributes.Add(attributeName, attributeValue);
                        }
                        else
                        {
                            attr.Value = attributeValue;
                        }
                    }
                    else
                    {
                        node.Attributes.Add(attributeName, attributeValue);
                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }

        public static string RemoveAttribute(this string htmlContent, string selector, string attributeName)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes(selector).ToList())
            {
                if (node.HasAttributes)
                {
                    var attr = node.Attributes[attributeName];
                    if (attr != null)
                    {
                        attr.Remove();
                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }

        public static string RemoveTableColumns(this string htmlContent, int[] columnsToRemove)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            if (doc.DocumentNode.SelectNodes("//tr") != null)
            {
                foreach (HtmlNode row in doc.DocumentNode.SelectNodes("//tr"))
                {
                    foreach (var i in columnsToRemove)
                    {
                        try
                        {
                            row.RemoveChild(row.SelectSingleNode("td[" + i + "]"));
                        }
                        catch (Exception exp) { }
                        finally { }

                        try
                        {
                            row.RemoveChild(row.SelectSingleNode("th[" + i + "]"));
                        }
                        catch (Exception exp) { }
                        finally { }
                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }

        public static string TrimContent(this string htmlContent, string queryPath)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            var node = doc.DocumentNode.SelectSingleNode(queryPath);
            if (node != null)
            {
                htmlContent = node.OuterHtml;
            }
            else
            {
                htmlContent =  "node not found: " + queryPath;
            }

            return htmlContent;
        }


        public static string RemoveStyleAttributes(this string htmlContent)
        {

            string[] attributesToIgnore = new string[] { "href" };

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);
            if (doc.DocumentNode.SelectNodes("//*") != null)
            {
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//*"))
                {
                    if (node.Attributes != null)
                    {
                        foreach (var attr in node.Attributes.Where(p => !attributesToIgnore.Contains(p.Name.ToLower())).ToList())
                        {
                            attr.Remove();
                        }
                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }

        public static string RemoveNonDigits(this string value)
        {
            return Regex.Replace(value, @"[^0-9]+", string.Empty, RegexOptions.Compiled);
        }

        public static string RemoveSpecialCharacters(this string value)
        {
            return Regex.Replace(value, @"[^0-9a-zA-Z\-_]+", string.Empty, RegexOptions.Compiled);
        }

        public static string ReplaceSpecialCharacters(this string value, string replacement)
        {
            return Regex.Replace(value, @"[^0-9a-zA-Z\-_ ]+", replacement, RegexOptions.Compiled);
        }

        public static string Chop(this string value, int length)
        {
            if (value.Length > length)
            {
                return value.Substring(0, length - 1);
            }

            return value;
        }

        public static string ToMD5Hash(this string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

    }
}