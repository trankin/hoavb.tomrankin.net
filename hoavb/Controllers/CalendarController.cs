﻿using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using hoavb.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace hoavb.Controllers
{
    public class CalendarController : Controller
    {
        //
        // GET: /Calendar/



        public ActionResult Index(string id, int? agelevel, string divisionlevel)
        {

            ViewBag.PageTitle = "Calendar Listing " + id;
            ViewBag.ActiveNav = "calendar_list";
            ViewBag.ActiveSubNav = id;
            Data data = new Data();

            var list = data.GetTournaments().Where(p => p.Season == id).ToList();

            if (agelevel.HasValue && String.IsNullOrEmpty(divisionlevel))
            {
                list = list.Where(p => p.Tournaments_AgeLevels.Select(f => f.AgeCode).Contains(agelevel.Value)).ToList();
            }

            if (!string.IsNullOrEmpty(divisionlevel) && agelevel.HasValue)
            {
                list = list.Where(p => p.Tournaments_AgeLevels.Where(f => f.AgeCode == agelevel.Value).Select(f => f.RestrictionLevel).Contains(divisionlevel)).ToList();
            }

            if (!string.IsNullOrEmpty(divisionlevel) && !agelevel.HasValue)
            {
                list = list.Where(p => p.Tournaments_AgeLevels.Select(f => f.RestrictionLevel).Contains(divisionlevel)).ToList();
            }

            return View("Index", list);
        }


        // id == season
        public ActionResult iCal(string id, int agelevel, string divisionlevel)
        {
            // Create a new iCalendar
            iCalendar iCal = new iCalendar();
            iCal.AddTimeZone(TimeZoneInfo.Local);


            Data data = new Data();
            var listing = data.GetTournaments().Where(p => p.Season == id && p.Tournaments_AgeLevels.Select(f => f.AgeCode).Contains(agelevel)).ToList();

            if (!string.IsNullOrEmpty(divisionlevel))
            {
                listing = listing.Where(p => p.Tournaments_AgeLevels.Where(f => f.AgeCode == agelevel).Select(f => f.RestrictionLevel).Contains(divisionlevel)).ToList();

            }

            //iCal.Name = "HOA Tournament Calendar: " + agelevel + " " + divisionlevel;


            foreach (var item in listing.OrderByDescending(p => p.dtStart))
            {
                // Create the event, and add it to the iCalendar
                Event evt = iCal.Create<Event>();

                // Set information about the event
                evt.Contacts.Add(item.Name);
                evt.Start = (new iCalDateTime(item.dtStart)).AddDays(0);
                evt.UID = item.ID;
                evt.End = (new iCalDateTime(item.dtEnd.AddDays(1))).AddDays(0);
                evt.Summary = item.Name + " " + String.Join(", ", item.Tournaments_AgeLevels.OrderBy(p => p.AgeCode).ThenBy(p => p.RestrictionLevel).Select(p => p.AgeDescription + p.RestrictionLevel).ToArray());
                evt.Description = "Contact: " + item.ContactName + "\r\n" +
                                  "Age Levels: " + String.Join(", ", item.Tournaments_AgeLevels.OrderBy(p => p.AgeCode).ThenBy(p => p.RestrictionLevel).Select(p => p.AgeDescription + " " + p.RestrictionDescription).ToArray()) + "\r\n" +
                                  "Fee: " + item.Fee;
                evt.Location = item.Site;
            }

            iCalendarSerializer serializer = new iCalendarSerializer();
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms);

            serializer.Serialize(iCal, sw.BaseStream, System.Text.Encoding.UTF8);

            sw.Flush();
            ms.Position = 0;
            var sr = new StreamReader(ms);
            var myStr = sr.ReadToEnd();

            sr.Close();
            sw.Close();


            var contentType = "text/calendar";
            var bytes = Encoding.UTF8.GetBytes(myStr);
            var result = new FileContentResult(bytes, contentType);
            result.FileDownloadName = "icaldata.ics";
            return result;

        }

    }
}
