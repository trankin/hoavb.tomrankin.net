﻿
using hoavb.Core;
using Services.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hoavb.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/


        public ActionResult ClearIndex()
        {
            SearchService ss = new SearchService();
            ss.ClearIndex();
            return Content(DateTime.Now.Ticks.ToString());
        }


        public ActionResult Optimize()
        {
            SearchService ss = new SearchService();
            ss.Optimize();
            return Content(DateTime.Now.Ticks.ToString());
        }


        public ActionResult Preview(string id)
        {
            string content = System.IO.File.ReadAllText(Utilities.MapPath("/Content/cache/" + id + ".html"));
            return View("Preview", (object)content);

        }

        public ActionResult Index(string keywords)
        {
            if (!String.IsNullOrEmpty(keywords))
            {
                SearchService searchService = new SearchService();
                searchService.PageNumber = 0;
                searchService.PageSize = 1000;
                var results = searchService.SearchUris(keywords, "");
                return View("Search", results);
            }

            return View("Search");
        }
    }
}
