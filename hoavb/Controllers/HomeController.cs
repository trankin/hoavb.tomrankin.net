﻿using hoavb.Services;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Services.Search;
using Datatables.Mvc;
using hoavb.Models.DataModel;
using System.Net.Mail;
using System.Net;

namespace hoavb.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        private static int counter = 0;

        public ActionResult Index()
        {
            return ClubList("2017/2018");
        }


        public ActionResult Preview(string id)
        {
            //using (datastoreEntities db = new datastoreEntities())
            //{
            //    return View(db.Documents.Where(p => p.ID == id).SingleOrDefault());
            //}
            return View();
        }


        public ActionResult IndexData()
        {
            SearchService ss = new SearchService();

            Services.DataRetrieval svc = new Services.DataRetrieval();
            svc.RefreshDataFromHOA(true);
            ss.Optimize();
            return Content(DateTime.Now.Ticks.ToString()) ;
        }

        public ActionResult CleanUpNames()
        {

            using (Data data = new Data())
            {

                foreach (var person in data.GetPersons().ToList())
                {
                    data.CleanUpPersonName(person);
                }

                data.SaveChanges();
            }


            return View();
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ReceiveRankingData()
        {
            try
            {
                string tmp = GetDocumentContents(Request).Replace("0=", "");
                string data = HttpUtility.UrlDecode(tmp);


                DataRetrieval svc = new DataRetrieval();

                svc.ProcessRankingData(data, "2017/2018");
            }
            catch (Exception exp) {
                SendEmailNotification("tom@tomrankin.net", "failed to process ranking data", "EOM");
            }
            return Content("");

        }


        private void SendEmailNotification(string to, string subject, string body)
        {
            SmtpClient client = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("system@sasquatchisreal.com", "nimdapassword")
            };


            var mm = new MailMessage("system@sasquatchisreal.com", to, subject, body);
            mm.IsBodyHtml = true;
            client.Send(mm);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ReceiveCalendarData() 
        {
            counter++;

            string tmp = GetDocumentContents(Request).Replace("0=","");
            string[] dataSets = tmp.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

            IList<String> idsAdded = new List<String>();

            foreach (var dataSet in dataSets) {
                string data = HttpUtility.UrlDecode(dataSet) ;

                data = data.Substring(data.IndexOf("<table class=\"sel_tbl\" width=\"100%\">"));
                data = data.Substring(0, data.IndexOf("</table>"));

                DataRetrieval svc = new DataRetrieval();
                var thoseAdded = svc.ProcessCalendarData(data, "2017/2018");
                idsAdded = idsAdded.Concat(thoseAdded).ToList();
            }

            using (Data data = new Data())
            {
                foreach (var tourney in data.GetTournaments().Where(p => p.Season == "2017/2018").ToList())
                {
                    if (!idsAdded.Contains(tourney.ID))
                    {
                        data.DeleteTournament(tourney);
                    }
                }
            }

            return Content("DONE");
        }

        private string GetDocumentContents(System.Web.HttpRequestBase Request)
        {
            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            return documentContents;
        }

        public ActionResult RefreshData()
        {

            Services.DataRetrieval svc = new Services.DataRetrieval();
            svc.RefreshDataFromHOA();
            return View();
        }

        public ActionResult Rankings(string season, int ageLevel)
        {
            Data data = new Data();

            var list = data.GetTeams().Where(p => p.Season == season && p.AgeCode == ageLevel && p.RankScore.HasValue).ToList();
            return View(list);
        }

        public ActionResult Person(string id)
        {
             Data data = new Data();

             return View(data.GetPerson(id));
        }

        public ActionResult PersonList()
        {
            ViewBag.PageTitle = "Person Search";
            ViewBag.ActiveNav = "person_search";

            Data data = new Data();

            return View();
        }

        [HttpPost]
        public ActionResult GetPersonsDataTable(DataTable dataTable, string id)
        {

            Data data = new Data();
            int totalPersonCount = data.GetTotalPersonCount();

            IQueryable<Person> personsResults;

            personsResults = data.GetPersonsAsQueryable();

            if (!string.IsNullOrEmpty(dataTable.sSearch))
            {
                personsResults = data.SearchPersons(dataTable.sSearch);// allUsers.Where(u => u.UserName.ToLower().Contains(dataTable.sSearch.ToLower()) || string.Join(", ", u.Companies).ToLower().Contains(dataTable.sSearch.ToLower()) || u.DisplayName.ToLower().Contains(dataTable.sSearch.ToLower())).ToList();
            }

            if (dataTable.iSortCols != null)
            {
                int sortcol = 0;
                DataTableSortDirection sortdir = DataTableSortDirection.Ascending;
                if (dataTable.iSortCols.Count > 0)
                {
                    sortcol = dataTable.iSortCols[0];
                    sortdir = dataTable.sSortDirs[0];
                }
                switch (sortcol)
                {
                    case 0:
                        if (sortdir == DataTableSortDirection.Descending)
                        {
                            personsResults = personsResults.OrderByDescending(f => f.LastName);
                        }
                        else
                        {
                            personsResults = personsResults.OrderBy(f => f.LastName);
                        }
                        break;
                    case 1:
                        if (sortdir == DataTableSortDirection.Descending)
                        {
                            personsResults = personsResults.OrderByDescending(f => f.FirstName);
                        }
                        else
                        {
                            personsResults = personsResults.OrderBy(f => f.FirstName);
                        }
                        break;
                    case 2:
                        if (sortdir == DataTableSortDirection.Descending)
                        {
                            personsResults = personsResults.OrderByDescending(f => f.ID);
                        }
                        else
                        {
                            personsResults = personsResults.OrderBy(f => f.ID);
                        }
                        break;
                    default:
                        if (sortdir == DataTableSortDirection.Descending)
                        {
                            personsResults = personsResults.OrderByDescending(f => f.LastName);
                        }
                        else
                        {
                            personsResults = personsResults.OrderBy(f => f.LastName);
                        }
                        break;

                }

            }

            var persons = personsResults.Skip(dataTable.iDisplayStart).Take(dataTable.iDisplayLength).ToList();

            List<List<string>> table = new List<List<string>>();
            if (persons != null && persons.Count > 0)
            {

                foreach (var u in persons)
                {
                    List<string> r = new List<string>();
                    r.Add(u.LastName);
                    r.Add(u.FirstName);
                    r.Add(u.CurrentClubAreaCode);
                    r.Add(u.ID);
                    r.Add(String.Join("<br />", u.Teams_Persons.OrderBy(p => p.Team.Season).Select(p =>
                        "<a href=\"/Home/TeamInfo/?id=" + p.TeamID + "\">" + p.Team.TeamLabel + "</a>"
                        
                        ).ToArray()));

                    //string act = "<a href=\"/Home/Person/" + u.ID + "\" class=\"btn btn-primary\">View</a>";
                    //r.Add(act);

                    table.Add(r);
                }
            }
            return new DataTableResult(dataTable.sEcho, totalPersonCount, personsResults.Count(), table);
        }

        public ActionResult RefreshTournamentCalendarOnly()
        {
            Services.DataRetrieval svc = new Services.DataRetrieval();
            svc.RefreshTournamentCalendarOnly();
            return View();
        }

        public ActionResult RefreshRankingsOnly()
        {
            Services.DataRetrieval svc = new Services.DataRetrieval();
            svc.RefreshTeamRankingDataOnly();
            return View();
        }


        public ActionResult TeamList(string id)
        {

            Data data = new Data();

            var list = data.GetTeams(id).ToList();
            return View(list);

        }


        public ActionResult TeamInfo(string id)
        {
            Data data = new Data();

            return View(data.GetTeam(id));

        }



        public ActionResult ClubList(string id)
        {

            ViewBag.PageTitle = "Club Listing " + id;
            ViewBag.ActiveNav = "club_list";
            ViewBag.ActiveSubNav = id;
            Data data = new Data();

            var list = data.GetClubSeasons().Where(p => p.Season == id).ToList();
            return View("ClubList", list);
        }






    }
}
