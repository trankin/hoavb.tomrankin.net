//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hoavb.Models.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tournaments_AgeLevels
    {
        public Tournaments_AgeLevels()
        {
            this.Teams_Tournaments = new HashSet<Teams_Tournaments>();
        }
    
        public string ID { get; set; }
        public string TournamentID { get; set; }
        public string AgeRestrictionCode { get; set; }
        public int AgeCode { get; set; }
        public string SexCode { get; set; }
        public string RestrictionLevel { get; set; }
        public string LeagueCode { get; set; }
        public string AgeDescription { get; set; }
        public string SexDescription { get; set; }
        public string RestrictionDescription { get; set; }
        public string LeagueDescription { get; set; }
        public int DBID { get; set; }
        public string Season { get; set; }
    
        public virtual Tournament Tournament { get; set; }
        public virtual ICollection<Teams_Tournaments> Teams_Tournaments { get; set; }
    }
}
