﻿using hoavb.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace hoavb.Services
{
    public class Data : IDisposable
    {
        private HOA_DATAEntities _db = null;

        public Data()
        {
            _db = new Models.DataModel.HOA_DATAEntities();
        }

        public ClubSeason GetClubSeason(string id)
        {
            return _db.ClubSeasons.Where(p => p.ID == id).SingleOrDefault();
        }

        public IList<ClubSeason> GetClubSeasons()
        {
            return _db.ClubSeasons.Include("Club").Include("Club_AgeLevels").ToList();
        }



        public ClubSeason CreateClubSeason(string id, string clubId)
        {
            var clubSeason = _db.ClubSeasons.Create();
            _db.ClubSeasons.Add(clubSeason);
            clubSeason.ID = id;
            clubSeason.ClubID = clubId;
            return clubSeason;
        }

        public Club GetClub(string id)
        {
            return _db.Clubs.Where(p => p.ID == id).SingleOrDefault();
        }

        public IList<Club> GetClubs()
        {
            return _db.Clubs.ToList();
        }


        public Club CreateClub(string id)
        {
            var club = _db.Clubs.Create();
            _db.Clubs.Add(club);
            club.ID = id;
            return club;
        }

        public Team GetTeam(string id)
        {
            return _db.Teams.Where(p => p.ID == id).SingleOrDefault();
        }

        public Team GetTeam(string name, string sexCode, int ageCode, string season)
        {
            return _db.Teams.Where(p => p.TeamName == name && p.SexCode == sexCode && p.AgeCode == ageCode && p.Season == season).SingleOrDefault();
        }

        public IList<Team> GetTeams(string clubSeasonId)
        {
            return _db.Teams.Where(p => p.SeasonID == clubSeasonId).ToList();
        }

        public Tournament GetTournament(string id)
        {
            return _db.Tournaments.Where(p => p.ID == id).SingleOrDefault();
        }

        public void DeleteTournament(Tournament tournament)
        {
             _db.Tournaments.Remove(tournament);
             _db.SaveChanges();
        }

        public Tournaments_AgeLevels GetTournamentRestriction(string id)
        {
            return _db.Tournaments_AgeLevels.Where(p => p.ID == id).SingleOrDefault();
        }


        public Tournaments_AgeLevels GetTournamentRestriction(int dbid, string season)
        {
            return _db.Tournaments_AgeLevels.Where(p => p.DBID == dbid && p.Season == season).SingleOrDefault();
        }

        public IList<Team> GetTeams()
        {
            return _db.Teams.ToList();
        }

        public IList<Tournaments_AgeLevels> GetRestrictions()
        {

            return _db.Tournaments_AgeLevels.ToList();
        }

        public Tournaments_AgeLevels CreateTournamentRestriction(string id, string tournamentId, int dbid, string season)
        {
            var restriction = _db.Tournaments_AgeLevels.Create();
            _db.Tournaments_AgeLevels.Add(restriction);
            restriction.ID = id;
            restriction.TournamentID = tournamentId;
            restriction.DBID = dbid;
            restriction.Season = season;

            return restriction;
        }

        public void ClearDuplicateTeams()
        {
            _db.ClearDuplicateTeams();
        }

        public Tournament CreateTournament(string id, string season)
        {
            var tourney = _db.Tournaments.Create();
            _db.Tournaments.Add(tourney);
            tourney.ID = id;
            tourney.Season = season;
            return tourney;
        }

        public Team CreateTeam(string clubSeasonId, string clubId, string id)
        {
            var team = _db.Teams.Create();
            _db.Teams.Add(team);
            team.ID = id;
            team.ClubID = clubId;
            team.SeasonID = clubSeasonId;
            return team;
        }


        public IQueryable<Person> SearchPersons(string search)
        {

            var persons = _db.Persons.AsQueryable();


            //strip out the Thomas "Tom" Rankin quoted Middle name.. leaving just Thomas Rankin
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"((""((?<token>.*?)(?<!\\)"")|(?<token>[\w]+))(\s)*)", options);
            var searchArray = (from Match m in regex.Matches(search)
                          where m.Groups["token"].Success
                          select m.Groups["token"].Value).ToArray();


            foreach (string s in searchArray)
            {
                string phrase = s.Replace("\"", "");
                persons = persons.Where(p => p.DisplayName.Contains(phrase) ||
                                        p.LastName.Contains(phrase) ||
                                        p.FirstName.Contains(phrase) ||
                                        p.ID.Contains(phrase) ||
                                        p.CurrentClubAreaCode.Contains(phrase) ||
                                        p.Teams_Persons.Any(f => f.Team.TeamLabel.Contains(phrase))
                                        );

            }

            return persons.ToList().AsQueryable();
        }

        public int GetTotalPersonCount()
        {
            return _db.Persons.Count();
        }

        public IList<Person> GetPersons()
        {
            return _db.Persons.ToList();
        }

        public IQueryable<Person> GetPersonsAsQueryable()
        {
            return _db.Persons;
        }


        public Person GetPerson(string id)
        {
            return _db.Persons.Where(p => p.ID == id).SingleOrDefault();
        }

        public Person CreatePerson(string id)
        {
            var person = _db.Persons.Create();
            _db.Persons.Add(person);
            person.ID = id;
            return person;
        }

        public Club_AgeLevels GetAgeLevel(string clubSeasonId, string ageLevelCode)
        {
            return _db.Club_AgeLevels.Where(p => p.ClubSeasonID == clubSeasonId && p.AgeLevelCode == ageLevelCode).SingleOrDefault();
        }


        public IList<Tournament> GetTournaments()
        {
            return _db.Tournaments.ToList();
        }

        public void AddAgeLevel(string clubSeasonId, string clubId, string ageLevelCode)
        {
            var existing = GetAgeLevel(clubSeasonId, ageLevelCode);
            if (existing == null)
            {
                existing = _db.Club_AgeLevels.Create();
                existing.ClubSeasonID = clubSeasonId;
                existing.ClubID = clubId;
                existing.AgeLevelCode = ageLevelCode;
                _db.Club_AgeLevels.Add(existing);
            }

            HydrateMetaInformation(existing);

            SaveChanges();
        }


        public void CleanUpPersonName(Person person)
        {
            person.DisplayName = person.DisplayName.Replace("    ", " ").Replace("   ", " ").Replace("  ", " ");
            string workingName = person.DisplayName;
            person.NickName = "";
            if (person.DisplayName.Contains("\""))
            {
                int start = workingName.IndexOf("\"") + 1;
                int length = workingName.LastIndexOf("\"") - start;

                person.NickName = workingName.Substring(start, length);
                workingName = workingName.Replace(" \"" + person.NickName + "\"", "");

            }

            person.FirstName = workingName.Substring(0, workingName.IndexOf(" ")).Trim();
            person.LastName = workingName.Substring(workingName.IndexOf(" ") + 1).Trim();

            if (person.NickName.Equals(person.FirstName, StringComparison.CurrentCultureIgnoreCase) ||
                person.NickName.Equals(person.LastName, StringComparison.CurrentCultureIgnoreCase) ||
                person.NickName.Equals(person.FirstName + " " + person.LastName, StringComparison.CurrentCultureIgnoreCase))
            {
                person.NickName = ""; // people don't read.
            }
        }

        public void HydrateMetaInformation(Team team)
        {
            team.SexCode = team.TeamCode.Substring(0, 1);
            team.LeagueCode = team.TeamCode.Substring(1, 1);
            team.AgeCode = int.Parse(team.TeamCode.Substring(2, 1));

            team.SexDescription = team.SexCode == "F" ? "Female" : "Male";
            team.LeagueDescription = team.LeagueCode == "J" ? "Juniors" : "Pro";
            team.AgeDescription = team.AgeCode + 10 + "'s";

        }

        public void HydrateMetaInformation(Club_AgeLevels ageLevel)
        {
            ageLevel.SexCode = ageLevel.AgeLevelCode.Substring(0, 1);
            ageLevel.LeagueCode = ageLevel.AgeLevelCode.Substring(1, 1);
            ageLevel.AgeCode = int.Parse(ageLevel.AgeLevelCode.Substring(2, 1));
            
            ageLevel.SexDescription = ageLevel.SexCode == "F" ? "Female" : "Male";
            ageLevel.LeagueDescription = ageLevel.LeagueCode == "J" ? "Juniors" : "Pro";
            ageLevel.AgeDescription = ageLevel.AgeCode + 10 + "'s";

            if (ageLevel.AgeLevelCode.Length > 3)
            {
                ageLevel.LevelCode = ageLevel.AgeLevelCode.Substring(3, 1);
                ageLevel.LevelDescription = ageLevel.LevelCode == "G" ? "Gold" : ageLevel.LevelCode == "S" ? "Silver" : "Bronze";
            }

        }

        public void HydrateMetaInformation(Tournaments_AgeLevels ageLevel, string levelText)
        {

            ageLevel.LeagueCode = "J";

            // special rule to handle hoas current execution of level 10 in the calendar
            if (levelText == "10")
            {
                ageLevel.AgeCode = 0;
                ageLevel.RestrictionDescription = "Open";
                ageLevel.RestrictionLevel = "O";
                ageLevel.LeagueDescription = "Juniors";
                ageLevel.AgeDescription = "10";
                ageLevel.SexCode = "F";
                ageLevel.SexDescription = "Female";
                ageLevel.AgeRestrictionCode = ageLevel.SexCode + ageLevel.LeagueCode + ageLevel.AgeCode + ageLevel.RestrictionLevel;

            }
            else
            {
                ageLevel.SexCode = levelText.StartsWith("F") ? "F" : "M";
                ageLevel.AgeCode = int.Parse(levelText.Substring(1, 2)) - 10;

                ageLevel.RestrictionDescription = levelText.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)[1];
                ageLevel.RestrictionDescription = ageLevel.RestrictionDescription.Replace("\n", "");

                if (ageLevel.RestrictionDescription == "Gold") { ageLevel.RestrictionLevel = "G"; }
                if (ageLevel.RestrictionDescription == "Silver") { ageLevel.RestrictionLevel = "S"; }
                if (ageLevel.RestrictionDescription == "Bronze") { ageLevel.RestrictionLevel = "B"; }
                if (ageLevel.RestrictionDescription == "Open") { ageLevel.RestrictionLevel = "O"; }

                ageLevel.SexDescription = ageLevel.SexCode == "F" ? "Female" : "Male";
                ageLevel.LeagueDescription = ageLevel.LeagueCode == "J" ? "Juniors" : "Pro";
                ageLevel.AgeDescription = (ageLevel.AgeCode + 10).ToString();

                ageLevel.AgeRestrictionCode = ageLevel.SexCode + ageLevel.LeagueCode + ageLevel.AgeCode + ageLevel.RestrictionLevel;

            }





        }

        public void DeleteRestriction(Tournaments_AgeLevels restriction)
        {
            _db.Tournaments_AgeLevels.Remove(restriction);
            _db.SaveChanges();
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
            _db = null;

        }
    }
}