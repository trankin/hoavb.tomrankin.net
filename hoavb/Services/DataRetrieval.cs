﻿using hoavb.Core;
using hoavb.Models.DataModel;
using HtmlAgilityPack.Samples;
using Microsoft.AspNet.SignalR;
using Services.Search;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace hoavb.Services
{
    public class DataRetrieval
    {
        private string crlf = "||@@||";
        public static string phpSessionID = "";
        string rankingPostData = "ApplyFilter=View&f_fltr_genF=on&f_fltr_genM=on&f_fltr_lvl0=on&f_fltr_lvl1=on&f_fltr_lvl2=on&f_fltr_lvl3=on&f_fltr_lvl4=on&f_fltr_lvl5=on&f_fltr_lvl6=on&f_fltr_lvl7=on&f_fltr_lvl8=on&f_fltr_divG=on&f_fltr_divS=on&f_fltr_divB=on";
        string calendarPostData = "TR3_TRNselCal_f1=TR3_TRNselCal_f1&referer=%2FTRN%2FTR3_TRNselCal.C.php&f_windowId=JrITGpd2hH&f_CalendarType=ViewEvents&f_TRNmgt_AGE=J&f_tdirlst=Array&f_status=A%2CC&f_CloseWindow=&f_qfltrJrITGpd2hH=+AND+dtl.t06_co_gender+IN+%28%27F%27%2C%27M%27%2C%27B%27%2C%27C%27%29+AND+dtl.t06_co_level+IN+%28%270%27%2C%271%27%2C%272%27%2C%273%27%2C%274%27%2C%275%27%2C%276%27%2C%277%27%2C%278%27%29+AND+dtl.t06_co_division+IN+%28%27Gold%27%2C%27Silver%27%2C%27Bronze%27%2C%27Open%27%29&f_db_offset=0&f_db_rtnrows=45&f_db_maxrow=45&f_fltr_trntypeI=on&f_fltr_trntypeO=on&f_fltr_genF=on&f_fltr_genM=on&f_fltr_lvl0=on&f_fltr_lvl1=on&f_fltr_lvl2=on&f_fltr_lvl3=on&f_fltr_lvl4=on&f_fltr_lvl5=on&f_fltr_lvl6=on&f_fltr_lvl7=on&f_fltr_lvl8=on&f_fltr_trn_divGold=on&f_fltr_trn_divSilver=on&f_fltr_trn_divBronze=on&f_fltr_date=ALL";
        private static bool _INDEX_ONLY = false;
        private static string nextWindowId = "";
        private static string windowIdMatcher = @"<input type=""hidden"" name=""f_windowId"" value=""";

        private int tourneyCount = 0;

        public void RefreshTeamRankingDataOnly()
        {
            phpSessionID = "";
            GetPHPSessionId("https://hai3.hoavb.org/WWW/TR3_RankingsJr.C.php"); 
            //RefreshTeamRankingData("https://hai3.hoavb.org/TRN/TR3_TRNrankRpt2.C.php?OUTSIDE_HAI3=YES", "2015/2016", 500);
        
        }


        public void RefreshTournamentCalendarOnly()
        {
            //phpSessionID = "";
            //GetPHPSessionId("https://2012-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //ProcessTournamentCalendar("https://2012-hai3.hoavb.org/H2/T_TRNsel.php", "2011/2012");

            //phpSessionID = "";
            //GetPHPSessionId("https://2013-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //ProcessTournamentCalendar("https://2013-hai3.hoavb.org/H2/T_TRNsel.php", "2012/2013");

            //phpSessionID = "";
            //GetPHPSessionId("https://2014-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //ProcessTournamentCalendar("https://2014-hai3.hoavb.org/H2/T_TRNsel.php", "2013/2014");


            //phpSessionID = "";
            //GetPHPSessionId("https://2015-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //ProcessTournamentCalendar("https://2015-hai3.hoavb.org/H2/T_TRNsel.php", "2014/2015");

            //GetPHPSessionId("https://2016-hai3.hoavb.org/WWW/TR3_EventCalJr.C.php");
            //ProcessTournamentCalendar("https://2016-hai3.hoavb.org/TRN/TR3_TRNselCal.C.php?OUTSIDE_HAI3=YES", "2015/2016");

            //GetPHPSessionId("https://hai3.hoavb.org/WWW/TR3_EventCalJr.C.php");
            //ProcessTournamentCalendar("https://2017-hai3.hoavb.org/TRN/TR3_TRNselCal.C.php?OUTSIDE_HAI3=YES", "2016/2017");

            GetPHPSessionId("https://hai3.hoavb.org/WWW/TR3_EventCalJr.C.php");
            ProcessTournamentCalendar("https://hai3.hoavb.org/TRN/TR3_TRNselCal.C.php?OUTSIDE_HAI3=YES", "2017/2018");
        }

        public void RefreshDataFromHOA(bool indexOnly = false)
        {

            using (Data data = new Data())
            {
                data.ClearDuplicateTeams();
            }


            _INDEX_ONLY = indexOnly;



            //phpSessionID = "";
            //GetPHPSessionId("https://2012-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //RefreshCoreClubData("https://2012-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2011/2012", false, 157784700);
            //RefreshCoreClubSeasonData("https://2012-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2011/2012", 157784700);
            //RefreshBasicTeamDataForAllClubs("https://2012-hai3.hoavb.org/H2/M_TEMsel.php?HFC=", "2011/2012", 157784700);
            //RefreshDetailedTeamInforForAllClubs("https://2012-hai3.hoavb.org/H2/M_TEMinfo.php?HFT=", "2011/2012", 157784700);
            //RefreshTeamRankingData("https://2012-hai3.hoavb.org/H2/T_TRNrankRpt2.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNrankRpt2", "2011/2012", 157784700);
            //ProcessFullAgeRankings("2011/2012");



            //phpSessionID = "";
            //GetPHPSessionId("https://2013-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //RefreshCoreClubData("https://2013-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2012/2013", false, 157784700);
            //RefreshCoreClubSeasonData("https://2013-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2012/2013", 157784700);
            //RefreshBasicTeamDataForAllClubs("https://2013-hai3.hoavb.org/H2/M_TEMsel.php?HFC=", "2012/2013", 157784700);
            //RefreshDetailedTeamInforForAllClubs("https://2013-hai3.hoavb.org/H2/M_TEMinfo.php?HFT=", "2012/2013", 157784700);
            //RefreshTeamRankingData("https://2013-hai3.hoavb.org/H2/T_TRNrankRpt2.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNrankRpt2", "2012/2013", 157784700);
            //ProcessFullAgeRankings("2012/2013");

            //phpSessionID = "";
            //GetPHPSessionId("https://2014-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //RefreshCoreClubData("https://2014-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2013/2014", false, 157784700);
            //RefreshCoreClubSeasonData("https://2014-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2013/2014", 157784700);
            //RefreshBasicTeamDataForAllClubs("https://2014-hai3.hoavb.org/H2/M_TEMsel.php?HFC=", "2013/2014", 157784700);
            //RefreshDetailedTeamInforForAllClubs("https://2014-hai3.hoavb.org/H2/M_TEMinfo.php?HFT=", "2013/2014", 157784700);
            //RefreshTeamRankingData("https://2014-hai3.hoavb.org/H2/T_TRNrankRpt2.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNrankRpt2", "2013/2014", 157784700);
            //ProcessFullAgeRankings("2013/2014");



            //phpSessionID = "";
            //GetPHPSessionId("https://2015-hai3.hoavb.org/H2/T_TRNsel.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNcnf");
            //RefreshCoreClubData("https://2015-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2014/2015", true, 86400);
            //RefreshCoreClubSeasonData("https://2015-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2014/2015", 86400);
            //RefreshBasicTeamDataForAllClubs("https://2015-hai3.hoavb.org/H2/M_TEMsel.php?HFC=", "2014/2015", 86400);
            //RefreshDetailedTeamInforForAllClubs("https://2015-hai3.hoavb.org/H2/M_TEMinfo.php?HFT=", "2014/2015", 86400);


            //RefreshTeamRankingData("https://2015-hai3.hoavb.org/H2/T_TRNrankRpt2.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNrankRpt2", "2014/2015", 86400);
            //ProcessFullAgeRankings("2014/2015");

            phpSessionID = "";
            //GetPHPSessionId("https://2016-hai3.hoavb.org/TRN/TR3_TRNselCal.C.php?OUTSIDE_HAI3=YES");
            //RefreshCoreClubData("https://2016-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2015/2016", true, 86400);
            //RefreshCoreClubSeasonData("https://2016-hai3.hoavb.org/H2/H_CLBAprRpt.php?T=A&A=J", "2015/2016", 86400);
            //RefreshBasicTeamDataForAllClubs("https://2016-hai3.hoavb.org/H2/M_TEMsel.php?HFC=", "2015/2016", 86400);
            //RefreshDetailedTeamInforForAllClubs("https://2016-hai3.hoavb.org/TRN/TR3_TMinfo.C.php?HFT=", "2015/2016", 86400);


            ////RefreshTeamRankingData("https://hai3.hoavb.org/TRN/TR3_TRNrankRpt2.C.php?OUTSIDE_HAI3=YES", "2015/2016", 86400);
            //ProcessFullAgeRankings("2015/2016");

            //GetPHPSessionId("https://2017-hai3.hoavb.org/TRN/TR3_TRNselCal.C.php?OUTSIDE_HAI3=YES");

            //RefreshCoreClubData("https://2017-hai3.hoavb.org/pvt/H3_CLBAprRptJr.C.php", "2016/2017", true, 86400);
            //RefreshCoreClubSeasonData("https://2017-hai3.hoavb.org/pvt/H3_CLBAprRptJr.C.php", "2016/2017", 86400);
            //RefreshBasicTeamDataForAllClubs("https://2017-hai3.hoavb.org/pvt/M_TEMsel.C.php?HFC=", "2016/2017", 86400);
            //RefreshDetailedTeamInforForAllClubs("https://2017-hai3.hoavb.org/TRN/TR3_TMinfo.C.php?HFT=", "2016/2017", 86400);

            //RefreshTeamRankingData("https://2017-hai3.hoavb.org/TRN/TR3_TRNrankRpt2.C.php?OUTSIDE_HAI3=YES", "2016/2017", 86400);
            //ProcessFullAgeRankings("2016/2017");

            GetPHPSessionId("https://hai3.hoavb.org/TRN/TR3_TRNselCal.C.php?OUTSIDE_HAI3=YES");

            RefreshCoreClubData("https://hai3.hoavb.org/pvt/H3_CLBAprRptJr.C.php", "2017/2018", true, 86400);
            RefreshCoreClubSeasonData("https://hai3.hoavb.org/pvt/H3_CLBAprRptJr.C.php", "2017/2018", 86400);
            RefreshBasicTeamDataForAllClubs("https://hai3.hoavb.org/pvt/M_TEMsel.C.php?HFC=", "2017/2018", 86400);
            RefreshDetailedTeamInforForAllClubs("https://hai3.hoavb.org/TRN/TR3_TMinfo.C.php?HFT=", "2017/2018", 86400);

            //RefreshTeamRankingData("https://hai3.hoavb.org/TRN/TR3_TRNrankRpt2.C.php?OUTSIDE_HAI3=YES", "2016/2017", 86400);
            //ProcessFullAgeRankings("2017/2018");

            SearchService searchService = new SearchService();
            searchService.Optimize();


        }


        private void ProcessTournamentCalendar(string baseURL, string season)
        {





            string webData = RetrieveContent(baseURL, 0, "POST", calendarPostData.Replace("JrITGpd2hH", nextWindowId) + "&ApplyFilter=View", "");
            string matcher = @"<form action=""/TRN/TR3_TRNselCal.C.php?OUTSIDE_HAI3=YES"" method=""post"" name=""TR3_TRNselCal_f2"">";
            


            string tmp = webData;
            tmp = tmp.Substring(tmp.IndexOf(windowIdMatcher) + windowIdMatcher.Length);
            nextWindowId = tmp.Substring(0, tmp.IndexOf("\""));

            webData = webData.Substring(webData.IndexOf(matcher) + matcher.Length);
            webData = webData.Substring(0, webData.IndexOf("</form>"));

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            doc.LoadHtml(webData);

            if (doc.DocumentNode.SelectNodes("//table") != null)
            {
                if(!_INDEX_ONLY)
                    ProcessCalendarData(webData, season);

                //Arbitrarily maxing out @ 100.. I would expect this to be max 3 - 4
                //we are short circuiting this below when we detect that there are no more pages to retrieve
                for (int i = 0; i < 100; i++)
                {
                    webData = RetrieveContent(baseURL, 0, "POST", calendarPostData.Replace("JrITGpd2hH", nextWindowId) + "&Next=Next");
                    webData = webData.Substring(webData.IndexOf(matcher) + matcher.Length);
                    webData = webData.Substring(0, webData.IndexOf("</form>"));
                    doc.LoadHtml(webData);
                    if (doc.DocumentNode.SelectNodes("//table") != null)
                    {
                        if (!_INDEX_ONLY)
                            ProcessCalendarData(webData, season);
                    }
                    else
                    {
                        //jump out of the loop
                        break;
                    }
                }
            }
        }

        public List<String> ProcessCalendarData(string webData, string season)
        {
            List<String> idsAdded = new List<string>();

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            for (int i = 0; i < 1000; i++)
            {
                webData = webData.Replace("idx=" + i + "&", "");
                webData = webData.Replace("T_TRNsch2.php?tno=" + i + "&A=J&dtlno=", "");
            }



            doc.LoadHtml(webData);
            if (doc.DocumentNode.SelectNodes("//table//tr[position()>1]") != null)
            {
                foreach (var row in doc.DocumentNode.SelectNodes("//table//tr[position()>1]"))
                {
                    tourneyCount++;
                    using (Data data = new Data())
                    {
                        HtmlToText toText = new HtmlToText();
                        string[] dt = row.SelectSingleNode("td[1]").InnerText.Replace(",", string.Empty).Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        string[] ageLevels = toText.ConvertHtml(row.SelectSingleNode("td[4]").InnerText).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim()).ToArray();
                        System.Diagnostics.Debugger.Log(1, "", dt.Length.ToString());
                        if (dt.Length == 3 || dt.Length == 4)
                        {
                            int startDay = 0;
                            int endDay = 0;
                            int year = 0;
                            if (dt[2].Contains("-"))
                            {
                                string[] days = dt[2].Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                                startDay = int.Parse(days[0]);

                                endDay = int.Parse(days[1]);
                                year = int.Parse(dt[3]);

                            }
                            else
                            {
                                startDay = int.Parse(dt[2]);
                                endDay = startDay;
                                year = int.Parse(dt[3]);
                            }
                            DateTime dtStart, dtEnd;

                            dtStart = DateTime.Parse(dt[1] + " " + startDay + ", " + year);
                            dtEnd = DateTime.Parse(dt[1] + " " + endDay + ", " + year);

                            if (endDay < startDay) dtEnd = dtEnd.AddMonths(1);

                            //adjust for the year overlap from Dec to Jan
                            if (dtEnd.Year > year)
                            {
                                dtStart = dtStart.AddYears(-1);
                                dtEnd = dtEnd.AddYears(-1);
                            }


                            string id = (dtStart.ToShortDateString() + "_" + dtEnd.ToShortDateString() + "_" + String.Join("_", ageLevels.OrderBy(p => p)) + "_" +  row.SelectSingleNode("td[4]").InnerText.Chop(215)).ReplaceSpecialCharacters("_").ToLower().Trim() + "_" + season;
                            id = id.ToMD5Hash();
                            idsAdded.Add(id);
                            var tournament = data.GetTournament(id);
                            if (tournament == null)
                            {
                                tournament = data.CreateTournament(id, season);
                            }
                            else
                            {
                                //System.Diagnostics.Debugger.Break();
                            }

                            tournament.dtStart = dtStart;
                            tournament.dtEnd = dtEnd;
                            if (!String.IsNullOrEmpty(row.SelectSingleNode("td[2]").InnerText))
                            {
                                tournament.Fee = int.Parse(row.SelectSingleNode("td[2]").InnerText);
                            }
                            else
                            {
                                tournament.Fee = -1;

                            }
                            tournament.Site = toText.ConvertHtml(row.SelectSingleNode("td[5]").InnerHtml);
                            tournament.DBID = 1;
                            tournament.ContactName = row.SelectSingleNode("td[6]").InnerText;
                            tournament.Name = row.SelectSingleNode("td[3]").InnerText;
                            data.SaveChanges();


                            //Let's delete the temp restrictions
                            if (tournament.Tournaments_AgeLevels != null && tournament.Tournaments_AgeLevels.Count > 0)
                            {
                                foreach (var rest in tournament.Tournaments_AgeLevels.Where(p => p.ID.Contains("_TEMP")).ToList())
                                {
                                    if (rest.TournamentID != null)
                                    {
                                        data.DeleteRestriction(rest);
                                    }
                                }
                            }
                            // Let's run through the age levels and persist them too.
                            List<string> found = new List<string>();
                            if (row.SelectNodes("td[4]/a") != null && row.SelectNodes("td[4]/a").Count >= 1)
                            {
                                    
                                foreach (var item in row.SelectNodes("td[4]/a"))
                                {
                                    string text = item.InnerText;
                                    found.Add(text);
                                    string eventDBID = item.Attributes["href"].Value.Replace("/TRN/TR3_TRNresults.C.php?dtlno=", "").Replace("/TRN/TR3_TRNcnf.C.php?dtlno=", "").Replace("/TRN/TR3_TRNschView.C.php?dtlno=", "").Replace("&amp;OUTSIDE_HAI3=YES", "").Replace("/TRN/TR4_BeachReg.C.php?dtlno=", "");


                                    string eventID = id + "_" + eventDBID;
                                    string ageLevel = item.InnerText;


                                    var tournamentRestriction = data.GetTournamentRestriction(eventID);
                                    if (tournamentRestriction == null && eventDBID != "#")
                                    {
                                        tournamentRestriction = data.CreateTournamentRestriction(eventID, id, int.Parse(eventDBID), season);
                                    }

                                    if (tournamentRestriction != null)
                                    {
                                        data.HydrateMetaInformation(tournamentRestriction, ageLevel);
                                        data.SaveChanges();
                                    }


                                }
                            }
                            // Loop through the original array so that we capture age level
                            // that don't have id's yet
                            foreach (var textDesc in ageLevels)
                            {
                                if (!found.Contains(textDesc) && !String.IsNullOrEmpty(textDesc))
                                {
                                    found.Add(textDesc);
                                    string eventDBID = "_" + textDesc.ReplaceSpecialCharacters("_") + "_TEMP";
                                    string eventID = id + "_" + eventDBID;
                                    string ageLevel = textDesc;

                                    var tournamentRestriction = data.GetTournamentRestriction(eventID);
                                    if (tournamentRestriction == null)
                                    {
                                        tournamentRestriction = data.CreateTournamentRestriction(eventID, id, -1, season);
                                    }

                                    data.HydrateMetaInformation(tournamentRestriction, ageLevel);
                                    data.SaveChanges();
                                }
                            }


                        }
                        else
                        {
                            //System.Diagnostics.Debugger.Launch();
                        }
                    }
                }
            }


            return idsAdded;

        }

        private void ProcessFullAgeRankings(string season)
        {

            using (Data data = new Data())
            {
                var teamList = data.GetTeams().Where(p => p.Season == season && p.RankScore.HasValue);
                foreach (var teamGroup in teamList.GroupBy(p => p.AgeCode))
                {
                    int overallRank = 1;
                    foreach (var team in teamGroup.OrderByDescending(p => p.RankScore).ThenBy(p => p.TeamName))
                    {
                        team.AgeLevelRank = overallRank;
                        team.AgeLevelTotalTeamCount = teamGroup.Count();
                        team.DivisionTotalTeamCount = teamGroup.Where(p => p.DivisionCode == team.DivisionCode).Count();
                        overallRank++;
                    }
                }
                data.SaveChanges();
            }
        }

        public void ProcessRankingData(string indata, string season)
        {
  
            string currentSexCode = "";
            int currentAgeCode = -1;
            string currentDivisionCode = "";

            string webData = indata;
            if (_INDEX_ONLY) return;

            webData = webData.TrimContent("//*[@id=\"inputbox\"]/table");
            webData = webData.Replace("&nbsp;", ""); // icky spaces
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(webData);
            if (doc.DocumentNode.SelectNodes("//table//tr[position()>1]") != null)
            {
                int fullCount = doc.DocumentNode.SelectNodes("//table//tbody//tr").Count();
                int i = 0;
                foreach (var row in doc.DocumentNode.SelectNodes("//table//tbody//tr"))
                {
                    i++;


                    // If the first column is empty then it's a header and we need to update our current levels
                    if (row.SelectSingleNode("th[1]") != null)
                    {
                        string code = row.SelectSingleNode("th[2]").InnerText;
                        currentSexCode = code[0].ToString();
                        currentAgeCode = int.Parse(code.Substring(1, 2)) - 10;

                        currentDivisionCode = code.Contains("Gold") ? "G" : code.Contains("Silver") ? "S" : "B";

                    }
                    //otherwise it's a regular row and we should retrieve the data and update the team.
                    else
                    {
                        string teamName = row.SelectSingleNode("td[2]").InnerText.Replace("\n", "");

                        using (Data data = new Data())
                        {
                            var team = data.GetTeam(teamName, currentSexCode, currentAgeCode, season);
                            if (team != null) // odd scenario here where some teams are showing up that aren't in the team listing
                            {
                                team.DivisionCode = currentDivisionCode;
                                team.DivisionDescription = currentDivisionCode == "G" ? "Gold" : currentDivisionCode == "S" ? "Silver" : "Bronze";
                                team.DivisionRank = int.Parse(row.SelectSingleNode("td[1]").InnerText);
                                team.RankScore = double.Parse(row.SelectSingleNode("td[3]").InnerText);
                                team.Wins = int.Parse(row.SelectSingleNode("td[4]").InnerText);
                                team.Loss = int.Parse(row.SelectSingleNode("td[5]").InnerText);
                                team.SchedScore = double.Parse(row.SelectSingleNode("td[6]").InnerText);
                                
                                data.SaveChanges();
                            }
                        }

                    }
                }
            }
        }

        private void RefreshDetailedTeamInforForAllClubs(string baseURL, string season, int cacheTime)
        {

            string[] teamList = null;

            using (Data data = new Data())
            {
                teamList = data.GetTeams().Where(p => p.Season == season).Select(p => p.ID).ToArray();
            }

            int fullCount = teamList.Count();
            int i = 0;



            foreach (var teamId in teamList)
            {
                if (teamId == "214_2015/2016")
                {
                    //System.Diagnostics.Debugger.Launch();
                }


                i++;

                string webData = RetrieveContent(baseURL + teamId, cacheTime, "GET", "");
                if (!_INDEX_ONLY)
                {

                    webData = webData.Replace("&nbsp;", " ");
                    //Hack fix for malformed html 
                    webData = webData.Replace("Right/Left Handed</td>", "Right/Left Handed</td></tr>");
                    webData = webData.TrimContent("//div[@id='inputbox']");
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(webData);

                    // Persist Person Information
                    if (doc.DocumentNode.SelectNodes("//table[1]//tbody//tr") != null)
                    {
                        using (Data data = new Data())
                        {
          
                            var team = data.GetTeam(teamId);

                            IList<string> personIDsAdded = new List<string>();

                            foreach (var row in doc.DocumentNode.SelectNodes("//table[1]//tbody//tr"))
                            {
                                string id = row.SelectSingleNode("td[1]").InnerText;
                                string trimmedID = id.Substring(0, id.Length - 1);

                                var person = data.GetPerson(trimmedID);
                                if (person == null)
                                {
                                    person = data.CreatePerson(trimmedID);
                                }

                                
                                person.DisplayName = row.SelectSingleNode("td[3]").InnerText.Replace("\n", "");
                                if (team.Club.ContactPhone != null && team.Club.ContactPhone.Length > 3)
                                {
                                    person.CurrentClubAreaCode = team.Club.ContactPhone.Substring(0, 3);
                                }


                                data.CleanUpPersonName(person);



                                string role = row.SelectSingleNode("td[2]").InnerText.Replace("\n", "");
                                string personType = id.Substring(id.Length - 1);

                                // This data is no longer present
                                /*
                                string uniform = row.SelectSingleNode("td[4]").InnerText.Replace("\n", "");
                                string position = row.SelectSingleNode("td[5]").InnerText.Replace("\n", "");
                                string height = row.SelectSingleNode("td[6]").InnerText.Replace("\n", "");
                                string hand = row.SelectSingleNode("td[7]").InnerText.Replace("\n", "");
                                 */

                                if (person != null)
                                {
                                    if(!team.Teams_Persons.Any(f => f.PersonID == person.ID))
                                    { 
                                        team.Teams_Persons.Add(new Teams_Persons { PersonID = person.ID, TeamID = team.ID, Role = role, AsPersonType = personType});//, Hand = hand, Height = height, Position = position, Uniform = uniform });
                                    }
                                    else
                                    {
                                        var teamPerson = team.Teams_Persons.Single(f => f.PersonID == person.ID);
                                        teamPerson.Role = role;
                                        teamPerson.AsPersonType = personType;
                                    }

                                    personIDsAdded.Add(person.ID);
                                }


                                data.SaveChanges();
                            }

                            foreach (var teamPerson in team.Teams_Persons.ToList())
                            {
                                if (!personIDsAdded.Contains(teamPerson.PersonID))
                                {
                                    team.Teams_Persons.Remove(teamPerson);
                                }
                            }

                            data.SaveChanges();

                        }
                    }
                }

            }



        }

        private void RefreshBasicTeamDataForAllClubs(string baseURL, string season, int cacheTime)
        {
            IList<ClubSeason> clubSeasons = null;
            using (Data data = new Data())
            {
                clubSeasons = data.GetClubSeasons().Where(p => p.Season == season).ToList();
            }

            #region Get the Basic Team Information Loaded
            int fullCount = clubSeasons.Count;
            int current = 0;

            foreach (var clubSeason in clubSeasons)
            {
                current++;
                string webData = RetrieveContent(baseURL + clubSeason.DBID, cacheTime, "GET", "");
                if (!_INDEX_ONLY)
                {

                    webData = webData.TrimContent("//table");
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(webData);
                    if (doc.DocumentNode.SelectNodes("/table/tbody/tr") != null)
                    {
                        foreach (var row in doc.DocumentNode.SelectNodes("/table/tbody/tr"))
                        {
                            int dbid = int.Parse(row.SelectSingleNode("td[2]/a").Attributes["href"].Value.Replace("/TRN/TR3_TMinfo.C.php?HFT=", ""));
                            string id = dbid + "_" + season;
                            using (Data data = new Data())
                            {
                                var team = data.GetTeam(id);
                                if (team == null)
                                {
                                    team = data.CreateTeam(clubSeason.ID, clubSeason.ClubID, id);
                                }
                                team.DBID = dbid;
                                team.ClubID = clubSeason.ClubID.ToUpper();
                                team.StatusCode = row.SelectSingleNode("td[1]").InnerText;
                                team.TeamCode = row.SelectSingleNode("td[2]").InnerText.Replace("\n", "");
                                team.TeamName = row.SelectSingleNode("td[3]").InnerText.Replace("\n", "");
                                team.ContactName = row.SelectSingleNode("td[4]").InnerText;
                                team.Season = season;
                                team.TeamLabel = team.Season + " " + team.TeamName;
                                data.HydrateMetaInformation(team);
                                team.ClubTeamSort = 0;
                                team.NextRefresh = DateTime.Today.AddDays(1);
                                try
                                {
                                    team.ClubTeamSort = int.Parse(team.TeamCode.Substring(team.TeamCode.IndexOf(clubSeason.ClubID) + clubSeason.ClubID.Length).Replace("HA", ""));
                                }
                                catch (Exception exp) { }

                                data.SaveChanges();
                            }
                        }
                    }
                }
            }
            #endregion


        }

        private void RefreshCoreClubData(string baseURL, string season, bool storePrimaryInfo, int cacheTime)
        {


            string webData = RetrieveContent(baseURL, cacheTime, "GET", "");
            if (_INDEX_ONLY) return;

            if (webData.Contains("<html"))
            {


                // let's parse out what we want
                webData = webData.TrimContent("//table//tbody");
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

                webData = webData.Replace("<br>", crlf).Replace("<br/>", crlf).Replace("<br />", crlf);

                doc.LoadHtml(webData);
                #region Persist the Core Club Info
                int fullCount = doc.DocumentNode.SelectNodes("//tr").Count;
                int current = 0;

                foreach (var row in doc.DocumentNode.SelectNodes("//tr"))
                {
                    current++;
                    using (Data data = new Data())
                    {
                        string clubName = row.SelectSingleNode("td[1]//strong").InnerText;
                        var regex = new Regex(Regex.Escape(clubName.ToLower()));
                        string tmp = row.SelectSingleNode("td[1]").InnerText.ToLower().Replace(crlf, "");
                        tmp = tmp.Replace("&nbsp;", "");
                        string id = regex.Replace(tmp, "", 1).Replace(crlf, "").ToUpper();

                        var club = data.GetClub(id);
                        if (club == null)
                        {
                            club = data.CreateClub(id);
                        }

                        club.ClubName = row.SelectSingleNode("td[1]//strong").InnerText;
                        if (storePrimaryInfo)
                        {
                            club.ContactName = row.SelectSingleNode("td[2]").InnerText;
                            club.ContactAddress = row.SelectSingleNode("td[3]").InnerText.Replace("||@@||", "\r\n");
                            //club.ContactPhone = row.SelectSingleNode("td[4]").InnerText;
                        }

                        data.SaveChanges();

                    }
                #endregion


                }
            }
        }


        private void RefreshCoreClubSeasonData(string baseURL, string season, int cacheTime)
        {

            string webData = RetrieveContent(baseURL, cacheTime, "GET", "");
            if (_INDEX_ONLY) return;

            if (webData.Contains("<html"))
            {
                // let's parse out what we want
                webData = webData.TrimContent("//table//tbody");
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

                webData = webData.Replace("<br>", crlf).Replace("<br/>", crlf).Replace("<br />", crlf);

                doc.LoadHtml(webData);
                #region Persist the Core Club Info
                int fullCount = doc.DocumentNode.SelectNodes("//tr").Count;
                int current = 0;
                foreach (var row in doc.DocumentNode.SelectNodes("//tr"))
                {

                    current++;
                    int dbid = int.Parse(row.SelectSingleNode("td[1]//a").Attributes["href"].Value.Replace("/pvt/M_TEMsel.C.php?HFC=", ""));
                    string id = dbid + "_" + season;

                    string clubName = row.SelectSingleNode("td[1]//strong").InnerText;
                    var regex = new Regex(Regex.Escape(clubName.ToLower()));
                    string tmp = row.SelectSingleNode("td[1]").InnerText.ToLower().Replace(crlf, "");
                    tmp = tmp.Replace("&nbsp;", "");
                    string clubid = regex.Replace(tmp, "", 1).Replace(crlf, "").ToUpper();
                    using (Data data = new Data())
                    {
                        var clubSeason = data.GetClubSeason(id);
                        if (clubSeason == null)
                        {
                            clubSeason = data.CreateClubSeason(id, clubid);
                        }
                        clubSeason.DBID = dbid;
                        clubSeason.Name = clubName;
                        clubSeason.ClubID = clubid;
                        clubSeason.Season = season;

                        //club.ContactEmail = row.SelectSingleNode("td[5]").InnerText;
                        string levels = row.SelectSingleNode("td[3]").InnerText;
                        if (!string.IsNullOrEmpty(levels))
                        {
                            string[] lvlArray = levels.Replace(" ", "").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                            foreach (var level in clubSeason.Club_AgeLevels.Where(p => !lvlArray.Contains(p.AgeLevelCode)).ToList())
                            {
                                clubSeason.Club_AgeLevels.Remove(level);
                            }

                            foreach (var lvl in lvlArray)
                            {
                                data.AddAgeLevel(clubSeason.ID, clubSeason.ClubID, lvl);
                            }
                        }
                        data.SaveChanges();

                    }
                #endregion


                }
            }
        }

        public static void IndexWebContents(string address, string data, string contents)
        {
            //return;
            SearchService searchService = new SearchService();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.OptionWriteEmptyNodes = true;
            doc.LoadHtml(contents);
            string title = "Unset";
            if (doc.DocumentNode != null && doc.DocumentNode.SelectSingleNode("//title") != null)
            {
                title = doc.DocumentNode.SelectSingleNode("//title").InnerText;
            }

            HtmlToText toText = new HtmlToText();
            string text = toText.ConvertHtml(contents);
            searchService.IndexDocument(address, title, (address + data).ToMD5Hash(), text, false, false);
        }

        public static string RetrieveContent(string address, int expirationLimit, string method, string data, string referrer = "")
        {
            string hashedFile = (address + data).ToMD5Hash() + ".html";
            string hashPath = Utilities.MapPath("/Content/cache/" + hashedFile);
            if (expirationLimit > 0 && System.IO.File.Exists(hashPath) && FileAge(hashPath) < expirationLimit)
            {

                string contents = System.IO.File.ReadAllText(hashPath);
                IndexWebContents(address, data, contents);
                return contents;
            }
            else
            {
                string contents = "";
                try
                {
                    if (method == "GET")
                    {
                        contents = RetrieveResponse(address, data, "GET", referrer);
                    }
                    else
                    {
                        contents = RetrieveResponse(address, data, "POST", referrer);
                    }
                    System.IO.File.WriteAllText(hashPath, contents);

                    IndexWebContents(address, data, contents);
                    
                    return contents;
                }
                catch (System.Net.WebException exp)
                {
                    if (System.IO.File.Exists(hashPath))
                    {
                        return System.IO.File.ReadAllText(hashPath);
                    }

                    return "Unable to retrieve requested content: " + address + "<br />" + exp.Message;
                }



            }


        }

        

        private static string RetrieveResponse(string address, string data, string method, string referrer = "")
        {

            Uri remoteUri = new Uri(address);

            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(address);
            httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";
            httpRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            httpRequest.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
            httpRequest.Headers.Add("Accept-Language", "en-US,en;q=0.8");
            httpRequest.Headers.Add("Origin", "https://hai3.hoavb.org");
            

            httpRequest.Method = method;


            httpRequest.CookieContainer = new CookieContainer();

            if (!String.IsNullOrEmpty(phpSessionID))
            {
                httpRequest.CookieContainer.Add(new System.Net.Cookie("PHPSESSID", phpSessionID, "/", remoteUri.Host));
            }

            if (!String.IsNullOrEmpty(referrer))
            {
                httpRequest.Referer = referrer;
            }


            if (method == "POST")
            {
                httpRequest.Referer = address;
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                byte[] bytedata = Encoding.UTF8.GetBytes(data);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();
            }




            HttpWebResponse httpWebResponse = (HttpWebResponse)httpRequest.GetResponse();


            Stream responseStream = httpWebResponse.GetResponseStream();

            StringBuilder sb = new StringBuilder();

            using (StreamReader reader =
            new StreamReader(responseStream, System.Text.Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    sb.Append(line);
                }
            }

            return sb.ToString();
        }

        private static string GetPHPSessionId(string baseURL)
        {
            if (String.IsNullOrEmpty(phpSessionID))
            {
                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(baseURL);
                httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpRequest.GetResponse();
                using (Stream stream = httpWebResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    String responseString = reader.ReadToEnd();

                    string tmp = responseString;
                    if (tmp.IndexOf(windowIdMatcher) > 0)
                    {
  
                        tmp = tmp.Substring(tmp.IndexOf(windowIdMatcher) + windowIdMatcher.Length);
                        nextWindowId = tmp.Substring(0, tmp.IndexOf("\""));
                    }
                }

                phpSessionID = httpWebResponse.Headers["Set-Cookie"].Replace("PHPSESSID=", "").Replace("; path=/", "");
                return phpSessionID;
            }
            return "";
        }

        private static double FileAge(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                DateTime fileCreatedDate = System.IO.File.GetLastWriteTime(filePath);
                return (DateTime.Now - fileCreatedDate).TotalSeconds;
            }

            throw (new FileNotFoundException());

        }

        

    }
}