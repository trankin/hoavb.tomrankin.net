﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hoavb.Core
{




    public class Utilities
    {

        // I'm adding it this way so I can add a different method if this doesn't get executed on the web later.
        public static string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }

        public static string ActiveNav(dynamic ViewBag, string nav_label)
        {
            if (ViewBag.ActiveNav == nav_label) return "active";
            return "";
        }

        public static string ActiveSubNav(dynamic ViewBag, string nav_label)
        {
            if (ViewBag.ActiveSubNav == nav_label) return "active";
            return "";
        }
    }
}