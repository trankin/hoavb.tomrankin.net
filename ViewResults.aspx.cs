﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

public partial class ViewResults : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sessionId = GetSessionId();

        Page.Title = Request.QueryString["title"];

        string poststring = "ApplyFilter=View&f_fltr_lvl" + Request.QueryString["f_fltr_lvl"] + "=on&f_fltr_div" + Request.QueryString["f_fltr_div"] + "=on&f_fltr_gen" + Request.QueryString["f_fltr_gen"] + "=on";
        HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create("https://hai3.hoavb.org/H2/T_TRNrankRpt2.php");

        httpRequest.Method = "POST";
        httpRequest.ContentType = "application/x-www-form-urlencoded";
        httpRequest.CookieContainer = new CookieContainer();
        httpRequest.CookieContainer.Add(new Cookie("PHPSESSID", sessionId, "/", "hai3.hoavb.org"));

        byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
        httpRequest.ContentLength = bytedata.Length;

        Stream requestStream = httpRequest.GetRequestStream();
        requestStream.Write(bytedata, 0, bytedata.Length);
        requestStream.Close();


        HttpWebResponse httpWebResponse =
        (HttpWebResponse)httpRequest.GetResponse();
        Stream responseStream = httpWebResponse.GetResponseStream();

        StringBuilder sb = new StringBuilder();

        using (StreamReader reader =
        new StreamReader(responseStream, System.Text.Encoding.UTF8))
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                sb.Append(line);
            }
        }

        litResults.Text = sb.ToString();
    }

    private string GetSessionId()
    {
        HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create("https://hai3.hoavb.org/H2/T_TRNrankRpt2.php?A=J&C=W&S=A,C&F=fltr&H=T_TRNrankRpt2");

        HttpWebResponse httpWebResponse = (HttpWebResponse)httpRequest.GetResponse();
        //PHPSESSID=455e2cbd7e52d83443cdcf150ec07b59; path=/
        return httpWebResponse.Headers["Set-Cookie"].Replace("PHPSESSID=", "").Replace("; path=/", "");
    }
}