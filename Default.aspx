﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <%
            foreach (var genderK in genders.Keys)
            {
                foreach (var ageK in ages.Keys)
                {
                    foreach (var typeK in types.Keys)
                    {
                        string title = genders[genderK] + " " + types[typeK] + " " + ages[ageK];
                        
                        
                        %><a href="ViewResults.aspx?f_fltr_gen=<%=genderK %>&f_fltr_lvl=<%=ageK %>&f_fltr_div=<%=typeK %>&title=<%= Server.UrlEncode(title) %>"><%= Server.HtmlEncode(title) %></a><br /><%
                    }
                }
            }    
        %>
    </div>
    </form>
</body>
</html>
