﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using Lucene.Net.Search.Vectorhighlight;
using Lucene.Net.Search;
using System.Text;
using System.Reflection;
using Lucene.Net.QueryParsers;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis;
using System.Collections;
using Lucene.Net;
using Lucene.Net.Index;
using Lucene.Net.Documents;
using Lucene.Net.Analysis.Snowball;



namespace Services.Search
{
    public class SearchService 
    {
        private static readonly object Locker = new object();
        private static Lucene.Net.Index.IndexReader _reader;
        private static Lucene.Net.Index.IndexWriter _writer;
        private static string _indexPath = "/App_Data/hoavb_data_index";
        private static bool _reOpenReader = false;
        private int _pageNumber = 1;
        private int _pageSize = 20;
        private int _phraseSearchSlop = 3;

        public int PageNumber { get { return _pageNumber; } set { if (value >= 0) { _pageNumber = value; } else { throw new ArgumentOutOfRangeException("PageNumber", "Page Number must be greater than zero."); } } }
        public int PageSize { get { return _pageSize; } set { if (value > 0 && value <= 1000) { _pageSize = value; } else { throw new ArgumentOutOfRangeException("PageSize", "Page size must be between 1 and 1000"); } } }
        
        delegate void SearchResultDelegate(TopDocs topDocs, IndexSearcher searcher, Lucene.Net.Search.Query query);

        private static Lucene.Net.Index.IndexWriter IndexWriter
        {
            get
            {

                if (_writer == null)
                {

                    string indexPath = HttpContext.Current.Server.MapPath(_indexPath);
                    bool createDirectoryFlag = !System.IO.Directory.Exists(indexPath);
                    Lucene.Net.Store.Directory indexDir = Lucene.Net.Store.FSDirectory.Open(new System.IO.DirectoryInfo(indexPath));
                    _writer = new IndexWriter(indexDir,
                                                            new SnowballAnalyzer(Lucene.Net.Util.Version.LUCENE_30, "English"),
                                                            createDirectoryFlag,
                                                            IndexWriter.MaxFieldLength.UNLIMITED);
                }
                return _writer;
            }
        }

        private static Lucene.Net.Index.IndexReader IndexReader
        {
            get
            {
                lock (Locker)
                {
                    if (_reader == null)
                    {
                        _reader = IndexWriter.GetReader();
                    }
                    else
                    {
                        if (_reOpenReader)
                        {
                            _reOpenReader = false;
                            var tmpRdr = _reader.Reopen();
                            _reader.Dispose();
                            _reader = tmpRdr;
                        }
                    }

                    return _reader;
                }
            }
        }

        public void Optimize()
        {
            IndexWriter.Commit();
            IndexWriter.Optimize();
        }

        public void ClearIndex()
        {
            if (_reader != null) IndexReader.Close();
            if (_writer != null) IndexWriter.Close(false);
            _reader = null;
            _writer = null;

            string indexPath = HttpContext.Current.Server.MapPath(_indexPath);

            if (Directory.Exists(indexPath))
            {
                Directory.Delete(indexPath, true);
            }

            _reOpenReader = true;


        }


        public void IndexDocument(string url, string title, string md5Hash, string webText, bool doReOpen = true, bool doCommit = true)
        {

            string docId = md5Hash;

            IndexWriter.DeleteDocuments(new Term("id", docId));
            IndexWriter.Commit();

            Lucene.Net.Documents.Document doc = new Lucene.Net.Documents.Document();


            doc.Add(new Field("id", docId, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));

            doc.Add(new Field("documenttype", "uri", Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS));
            doc.Add(new Field("uriid", md5Hash, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
            doc.Add(new Field("url", url ?? "", Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("title", title ?? "", Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("content", webText ?? "", Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS));




            IndexWriter.UpdateDocument(new Term("id", docId), doc);
            if (doCommit)
            {
                IndexWriter.Commit();
            }
            if (doReOpen)
            {
                _reOpenReader = true;
            }
        }


        public UriSearchResult SearchUris(string keywords, string preFilterQueryText)
        {

            string[] searchFields = new string[] {  "documenttype",
                                                    "uriid",
                                                    "url",
                                                    "content",
                                                    "title" };

            Occur[] fieldClauses = new Occur[] {    Occur.SHOULD, 
                                                    Occur.SHOULD, 
                                                    Occur.SHOULD, 
                                                    Occur.SHOULD, 
                                                    Occur.SHOULD };

            if (keywords == null) keywords = "";

            UriSearchResult results = new UriSearchResult();
            results.ResultItemList = new List<UriResultItem>();

            string queryText = FormatQuery(keywords);

            SearchResultDelegate del = (topDocs, searcher, query) =>
            {
                results.MaxScore = topDocs.MaxScore;
                results.TotalHits = topDocs.TotalHits;
                results.PageCount = (results.TotalHits / _pageSize) + (results.TotalHits % _pageSize == 0 ? 0 : 1);
                results.PageSize = _pageSize;
                results.CurrentPageNumber = _pageNumber;
                if (_pageNumber > results.PageCount) results.CurrentPageNumber = results.PageCount;

                int resultPosition = (results.CurrentPageNumber * _pageSize) - _pageSize + 1;
                foreach (var item in topDocs.ScoreDocs.Skip((results.CurrentPageNumber * _pageSize) - _pageSize).Take(_pageSize))
                {
                    var doc = searcher.Doc(item.Doc);
                    var value = doc.GetField("uriid");
                    string uriid = value != null ? value.StringValue : "";
                    string title = doc.GetField("title") != null ? doc.GetField("title").StringValue ?? "" : "";
                    string url = doc.GetField("url") != null ? doc.GetField("url").StringValue ?? "" : "";
                    var highlighter = new FastVectorHighlighter();
                    string[] highlights = highlighter.GetBestFragments(highlighter.GetFieldQuery(query), IndexReader, item.Doc, "content", 320, 3);
                    string highlight = "";

                    if (highlights != null && highlights.Length > 0)
                    {
                        highlight = String.Join("<br />", highlights);
                    }


                    results.ResultItemList.Add(new UriResultItem { Title = title, URL = url, UriID = uriid, Rank = item.Score, Highlight = highlight ?? "" });

                    

                }
            };

            PerformSearch(queryText, preFilterQueryText, searchFields, fieldClauses, del);

            return results;

        }

        private void PerformSearch(string queryText, string preFilterQueryText, string[] searchFields, Occur[] fieldClauses, SearchResultDelegate searchDelegate)
        {

            Analyzer analyzer = new Lucene.Net.Analysis.Snowball.SnowballAnalyzer(Lucene.Net.Util.Version.LUCENE_30, "English");

            if ((queryText == null || queryText.Equals(string.Empty)) && (preFilterQueryText != null && !preFilterQueryText.Equals(string.Empty)))
            {
                queryText = preFilterQueryText;
                preFilterQueryText = "";
                analyzer = new KeywordAnalyzer();
            }

            Query query = ParseQuery(queryText, searchFields, fieldClauses, analyzer);

            IndexSearcher _searcher = new IndexSearcher(IndexReader);
            // Perform the Search

            TopDocs topDocs;
            if (!preFilterQueryText.Trim().Equals(String.Empty))
            {
                Filter queryFilter = ParsePreFilter(preFilterQueryText);
                topDocs = _searcher.Search(query, queryFilter, 1000);
            }
            else
            {
                topDocs = _searcher.Search(query, 1000);
            }

            if (topDocs.TotalHits != 0)
            {
                searchDelegate(topDocs, _searcher, query);
            }

            _searcher.Dispose();

        }

        #region Helpers
        private Query ParseQuery(string queryText, string[] searchFields, Occur[] fieldClauses, Analyzer analyzer)
        {
            return MultiFieldQueryParser.Parse(Lucene.Net.Util.Version.LUCENE_30, queryText, searchFields, fieldClauses, analyzer);
        }

        private Filter ParsePreFilter(string preFilterQueryText)
        {
            return new QueryWrapperFilter(new QueryParser(Lucene.Net.Util.Version.LUCENE_30, "content", new KeywordAnalyzer()).Parse(preFilterQueryText.Trim()));
        }

        private static int CharCount(string s, char c)
        {
            return s.Split(c).Length - 1;
        }
        
        
        private string FormatQuery(string query)
        {

            query = query.Replace("\\", "\\\\");

            string[] specialValues = new string[] { "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "~", "?", ":" };

            // Not allowed to start with a wildcard.
            if (query.StartsWith("*")) query = query.Substring(1);

            foreach (string val in specialValues)
            {
                query = query.Replace(val, "\\" + val);
            }

            if (CharCount(query, '"') % 2 == 1)
            {
                query.Replace("\"", "\\\"");
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                int quoteCount = 0;
                foreach (char c in query.ToArray())
                {
                    sb.Append(c);
                    if (c == '"')
                    {
                        quoteCount++;
                        if (quoteCount % 2 == 0)
                        {
                            sb.Append("~");
                            sb.Append(_phraseSearchSlop.ToString());
                            quoteCount = 0;
                        }
                    }
                }

                return sb.ToString();
            }


            return query;
        }
        #endregion
    }




    public class UriResultItem
    {
        public string UriID { get; set; }
        public string DocumentID { get; set; }
        public float Rank { get; set; }
        public string Highlight { get; set; }
        public string URL { get; set; }
        public string Title { get; set; }
    }

    public class UriSearchResult
    {
        public UriSearchResult()
        {
            TotalHits = 0;
            CurrentPageNumber = 0;
            PageCount = 0;
            MaxScore = 0f;
            PageSize = 0;
            ResultItemList = new List<UriResultItem>();

        }

        public int TotalHits { get; set; }
        public int CurrentPageNumber { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public float MaxScore { get; set; }

        public List<UriResultItem> ResultItemList { get; set; }

    }
}
