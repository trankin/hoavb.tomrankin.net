﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    public Dictionary<string, string> types = new Dictionary<string, string>();
    public Dictionary<string, string> ages = new Dictionary<string, string>();
    public Dictionary<string, string> genders = new Dictionary<string, string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        types = new Dictionary<string, string> { { "G", "Gold" }, { "S", "Silver" }, { "B", "Bronze" } };
        ages = new Dictionary<string, string> { { "0", "10's" }, 
                                                { "1", "11's" }, 
                                                { "2", "12's" }, 
                                                { "3", "13's" }, 
                                                { "4", "14's" }, 
                                                { "5", "15's" }, 
                                                { "6", "16's" }, 
                                                { "7", "17's" }, 
                                                { "8", "18's" } };
        genders = new Dictionary<string, string> { { "F", "Girls" }, { "M", "Boys" }};

    }
}